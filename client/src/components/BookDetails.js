import React from 'react';
import { getBookQuery } from '../queries/quries';
import { graphql } from 'react-apollo';


function BookDetails(props) {
    const displayBookDetails = () => {
        const { name, genre } = props.data.book;

        return (
            <>
                <h2>Book Detail: </h2>
                <p>Name of book: { name }</p>
                <p>Genre: { genre }</p>

                { otherAuthorBooks(props) }
            </>
        )
    }

    return (
        <section id="books-detail">
            {  props.data.book ? displayBookDetails() : null }
        </section> 

    );
}

const otherAuthorBooks = (props) => {
    if (props.data.book.author !== null ) {
        const { books, name, age } = props.data.book.author;

        return (
            <>
                <h3>Author Details: </h3>
                <p>Author name:  { name}</p>
                <p>Author age: { age }</p>
                <h4>OtherBooks written</h4>
                <ul>
                    <li>Books list:</li>
                        {
                            books.map((item, index) => {
                            return (
                                <li key={index} >
                                    <p>{ item.name }</p>
                                    <p>{ item.genre }</p>
                                </li>
                            )
                        })
                    }
                </ul>
            </>
        )
    }
}

export default graphql(getBookQuery, {
    options: (props) => {
        return {
            variables: {
                id: props.bookId
            }
        }
    }
})(BookDetails);
