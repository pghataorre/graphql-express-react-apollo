import React, {useState} from 'react';
import { graphql } from 'react-apollo';
import {flowRight as compose} from 'lodash';
import { getAuthorsQuery, addBookMutation, getBooksQuery } from '../queries/quries';

function AddBookForm(props) {
    const [formVals, setFormVals] = useState({});
    const { loading, authors } = props.getAuthorsQuery;
    const displaySelectOptions =() => {
        if (!loading) {
            return authors.map((item) => {
                return (<option key={ item.id } value={ item.id }>{ item.name }</option>)
            });
        } else {
            return null;
        }
    } 

    const handleSubmit = (event) => {
        event.preventDefault();

        const formVals = {
            bookName: event.currentTarget.elements[0].value,
            bookGenre: event.currentTarget.elements[1].value,
            authorId: event.currentTarget.elements[2].value
        }

        props.addBookMutation({
            variables: {
                name: formVals.bookName,
                genre: formVals.bookGenre,
                authorId: formVals.authorId
            },
            refetchQueries: [{ query: getBooksQuery}]
        })

        setFormVals(formVals);
    }

    return (
        <section>
            <hr />
            <form method="post" onSubmit={ (event) => handleSubmit(event) }>
                <div>
                    <label htmlFor="new-book">New Book name:</label>
                    <input type="text" id="new-book" required/>
                </div>
                <div>
                    <label htmlFor="new-genre">New Book Genre:</label>
                    <input type="text" id="new-genre" required/>
                </div>
                <div>
                    <label htmlFor="author-select">Author:</label>
                    <select id="author-select" required>
                        { displaySelectOptions() }
                    </select>
                </div>
                <div>
                    <button>Add Book</button>
                </div>
            </form>
        </section>
    )
}

export default compose(
    graphql(getAuthorsQuery, { name: 'getAuthorsQuery' }),
    graphql(addBookMutation, { name: 'addBookMutation' })
)(AddBookForm);
