import React, {useState} from 'react';
import { getBooksQuery } from '../queries/quries';
import { graphql } from 'react-apollo';

import BookDetails from './BookDetails';

function Booklist(props) {
    const [bookId, setBookId] = useState('');


    const displayList = () => {
        const { loading, books } = props.data;
        const getId = (id) => {
            setBookId(id);
        }
        
        if (!loading) {
            return books.map((item, index) => {
                return (
                    <li key={ index } onClick={ () => getId(item.id) }>
                        <p>Book name: { item.name }</p>
                        <p>Genre: { item.genre }</p>
                    </li>
                )
            });
        } else {
            return (<li>LOADING . . . </li>);
        }
    }

    return (
        <>
            <section>
                <ul id="book-list">
                    { displayList() }
                </ul>
            </section>
            <section>
                <BookDetails bookId={ bookId }/>
            </section>
        </>
    );
}

export default graphql(getBooksQuery)(Booklist);
