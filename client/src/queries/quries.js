import { gql } from 'apollo-boost';

const getAuthorsQuery = gql`
    {
        authors{
            name
            id
        }
    }
`;

// Get Book ONLY Single
const getBookQuery = gql`
query($id: ID!) {
    book(id: $id) {
        id
        name
        genre
        author{
            id
            name
            age
            books {
                name
                id
            }
        }
    }
}`;

// Get Books Many
const getBooksQuery = gql`
    {
        books{
            id
            name
            genre
        }
    }
`;

const addBookMutation = gql`
    mutation($name: String!, $genre: String!, $authorId: Int!) {
        addBook(name:$name, genre: $genre, authorId: $authorId){
            id
            name
        }
    }
`;

export {getBookQuery, getAuthorsQuery, getBooksQuery, addBookMutation};
