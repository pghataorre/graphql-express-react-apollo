import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider}  from  'react-apollo';


import BookList from './components/Booklist';
import AddBookForm from './components/AddBookForm';

const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql'
})

function App() {
  return (
    <ApolloProvider client={ client }>
      <div id="main">
        <h1>Ninja's Reading List</h1>
        <BookList />
        <AddBookForm />
      </div>
    </ApolloProvider>
  );
}

export default App;
